<?php
/**
 * Plugin Name: WooCommerce Netcash Gateway
 * Plugin URI: http://kcdipesh.com.np/woocommerce-netcash-plugin/
 * Description: Adds the Netcash Payment Gateway to your WooCommerce store, allowing customers to securely save their credit card to their account for use with single purchases, pre-orders, subscriptions, and more!
 * Author: Dipesh KC
 * Author URI: http://kcdipesh.com.np
 * Version: 1.0.0
 * Text Domain: 
 * Domain Path: 
 *
 * Copyright: (c) 2010-2014 kclabs, Inc. (caseydipesh@gmail.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Gateway-NetCash
 * @author    Dipesh
 * @category  Gateway
 * @copyright Copyright (c) 2012-2014, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Required functions
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}


// WC active check
if ( ! is_woocommerce_active() ) {
	return;
}

// Required library class
if ( ! class_exists( 'SV_WC_Framework_Bootstrap' ) ) {
	require_once( 'lib/skyverge/woocommerce/class-sv-wc-framework-bootstrap.php' );
}

	add_action( 'plugins_loaded', 'init_wp_kc_netcash_gateway_class' );
	add_filter( 'woocommerce_payment_gateways', 'add_wp_kc_netcash_gateway_class' );

	function init_wp_kc_netcash_gateway_class() {
		include_once('wc_kc_gateway.php');
	}

	function add_wp_kc_netcash_gateway_class( $methods ) {
		$methods[] = 'WC_KC_NetCash'; 
		return $methods;
	}

//$GLOBALS['wc_kc_netcash'] = new WC_KC_NetCash();
