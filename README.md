## WooCommerce Netcash Gateway

Author: Dipesh KC
Tags: woocommerce
Requires at least: 3.5
Tested up to: 3.8
Requires WooCommerce at least: 2.0
Tested WooCommerce up to: 2.1

Adds the Netcash Payment Gateway to your WooCommerce store, allowing customers to securely save their credit card to their account for use with single purchases, pre-orders, subscriptions, and more!

### Installation

1. Upload the entire 'woocommerce-gateway-braintree' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
